\chapter{Technical and Mathematical Background} \label{chap:background}

\section{LiDAR and LRF}
\begin{wrapfigure}{r}{0.4\textwidth}
	\centering
	\begin{minipage}{0.4\textwidth}
		\centering
		\includegraphics[width=0.66\textwidth]{fig/laser_range_finder}
		\caption{Top-down view on a laser range finder emitting its laser beams. The beams (gray, dashed lines) point into different directions, defined by the aperture angle $\gamma$. Some beams hit an obstacle, depicted as black line at a point $p$ and are reflected from there. This enables the scanner to calculate the distance $d$ to the obstacle, based on the time of flight of the emitted signal. The measurements lie in the x-y plane of the sensor frame, constructed by orthonormal vectors $x$, $y$ and $z$.}
		\label{fig:lrf}
	\end{minipage}
\end{wrapfigure}
\textit{Light detection and ranging} (LiDAR) or \textit{laser range finder} (LRF) devices are \textit{time-of-flight} sensors that sample surfaces of their surroundings. Time-of-flight in this case means that they emit a laser beam, usually in a frequency outside the visible spectrum and measure the time elapsed between emission and the reception of its reflection. Using the constant speed of light the distance $d$ to the reflecting sample point $p$ on a surface can be calculated (see Fig.~\ref{fig:lrf}). Besides $d$, Fig.~\ref{fig:lrf} shows that several beams (gray, dashed lines) with known aperture angle $\gamma$ w.r.t. the sensor's coordinate system are emitted. Usually the rays are not emitted simultaneously, but subsequently in a sweep from a \ang{0} angle (point along x-axis) to the maximum aperture angle $\max(\gamma)$, in steps which define the angular resolution of an LRF. Both maximum aperture angle and resolution depend on the actual sensor construction. As Fig.~\ref{fig:lrf} suggests, such sensors are only able to scan points on a single scanning plane. However, the measured $d$, and $\gamma$ can be used to represent a 2D point:
\begin{equation}
	\vec{p} = 
	\begin{bmatrix}
	x \\
	y
	\end{bmatrix}
	=
	\begin{bmatrix}
	d*\cos(\gamma) \\
	d*\sin(\gamma)
	\end{bmatrix}
	,\ \ \ \vec{p} \in \mathbb{R}^{2\times1}
\end{equation} 
\begin{wrapfigure}{r}{0.5\textwidth}
	\centering
	\begin{minipage}{0.5\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig/light_img_det_ranging}
		\caption{Single exemplary tilted 2D LRF of a multi-beam LiDAR construction. The LRF frame is defined by $[\vec{x}' \vec{y}' \vec{v}']$ and is tilted about an angle $\alpha$ around the x-axis of the LiDAR frame. The emitted beams hitting the obstacle plane at a point $p$ now, result in measurements of the distance $d$ and the aperture-angle $\gamma$, like done by an ordinary 2D LRF extended by the tilting angle $\alpha$.}
		\label{fig:lidar}
	\end{minipage}
\end{wrapfigure}
The restriction to a 2D perception is a great disadvantage in some use cases. To tackle this, multi-beam LiDAR systems arrange several 2D LRF with different tilting angles. Such an arrangement is depicted in Fig.~\ref{fig:lidar}. Only a single exemplary tilted LRF is shown to simplify the construction. It is tilted around the x-axis of the LiDAR frame by an angle of $\alpha$. This sensor frame is constructed from the orthonormal $x$, $y$ and $z$ vectors. In this way, the measurement of a single beam is extended by an azimuth angle $\alpha$, which corresponds to the tilting of a specific 2D LRF. The additional information enables the sensor to represent the measurements as 3D points, using the aperture angle $\gamma$, the azimuth angle $\alpha$ and the measured distance $d$.
\begin{equation}
	\vec{p} = 
	\begin{bmatrix}
	x \\
	y \\
	z
	\end{bmatrix}
	=
	\begin{bmatrix}
	d*\sin(\alpha)*\cos(\gamma) \\
	d*\sin(\alpha)*\sin(\gamma) \\
	d*\cos(\alpha)
	\end{bmatrix}
	,\ \ \ \vec{p} \in \mathbb{R}^{3\times1}
\end{equation} 
The accumulation of these individually sensed 3D points in a set results in a 3D point cloud sampling the environment of the LiDAR with respect to the sensor frame.

\section{3D Point Cloud}
In general, a point cloud is an unordered set of data points of equal, but arbitrary dimensions. However, in the context of robotic perception, point clouds usually denote sets of three-dimensional data points in $\mathbb{R}^{3\times1}$. The facts, that the data points potentially have no specific order and come without a defined neighborhood, have a major impact for many algorithms. For example in region growing methods this comes unhandy, because neighbors with similar properties are processed, but can not intuitively distinguished. This is different from 2D images where the pixel neighbors are defined by a grid structure. Also changing a point's position in a data structure, containing the 3D data, will not affect the point cloud as a whole. Such point clouds often result from the surface sampling of a sensor's surroundings. The sample points are usually represented as 3D vectors, encoding the Cartesian coordinates of a sample, relative to the sensor center. Such measurements are done by several sensor types, for example time-of-flight sensors like LiDARs or Radar, structured light sensors like the Kinect camera, stereo camera systems or structure from motion methods. Depending on the sensor or processing methods, the raw data vectors can be of higher than three dimensions, holding additional information. Such can be surface normals and curvature, reflection intensity (comparable to gray-scale pixel values in images), RGB color or other information. 

\section{Least Squares fitting}
To fit a model $f(x) = \mathbb{C}^M \longmapsto \mathbb{C}^N$ to a set of data points $D = {(x_i, y_i)},\ x_i \in \mathbb{C}^N,\ y_i \in \mathbb{C}^M,\ i = 0,1,2,...,K-1$, using the least squares method published in \citep{stigler1981},  the overall difference between the model and the data needs to be minimized, with respect to the model's coefficients. This process is called regression. Datasets are often acquired by simulations or sensor measurements, such as the point clouds produced by a LiDAR. In least squares fitting, the definition of a so-called residual function $d(f(x_i), y_i) = f(x_i) - y_i$, encoding the individual point-to-model distance needs to be defined to implement the regression. The residual is applied to each data tuple $(x_i, y_i) \in D$. To calculate an overall error, depending on the set of model coefficients $C \in \mathbb{C}^L$, all residuals need to be aggregated. By simply summing them up, those with different direction, i.e. with opposite sign, but same magnitude would annihilate each other. Such sample pairs would not contribute to the overall error. To tackle this, the individual distances are squared before the summation. Moreover, this way the residuals are implicitly weighted stronger the higher their magnitude is:
\begin{align}
	E(C) &= \sum_{i=0}^{K-1} d(f(x_i), y_i)^2 \\
         &= \sum_{i=0}^{K-1} (f(x_i)-y_i)^2.
\end{align}
The model coefficients minimizing $E(C)$ fit the observed data the best.
\begin{equation}
	C^{\ast} = argmin(E(C))
\end{equation}
The sum of squared residuals can also be differentiated at least one once, since the individual summands does not depend on each other. This makes even minimization methods, based on the derivatives, e.g. Regula Falsi applicable.

\section{Grid Search}
The optimization of functions with variants of gradient decent methods or other local optimizers is very common. In this cases converging against the global optimum is never guaranteed. There are some global optimizers trying to handle this, but many of them have a randomized search strategy or just apply local optimization from starting points derived from some heuristic. A reliable, simple, but computationally quite expensive way to avoid local optima is to perform a grid search. It builds a multidimensional grid of the function arguments. This is either done on categorical or a discretization of continuously defined arguments. With a high resolution of the discretization and especially for functions with arguments of higher dimensionality, this gets intractable very fast. Such a brute-force-search can be made more efficient by increasing the step size used for the discretization. Apart from that, the search space can be limited, i.e.\ the possible argument values are restricted to a specific range. However, this requires a-priori knowledge about the function, in order to successfully choose ranges that contain the searched, global optimum. The problem's dimensionality, i.e. the amount of its arguments have the biggest influence on the search complexity. Reducing it by reformulating the function or dividing it into several subproblems with lower dimensionality could make even high-dimensional problems tractable to optimize, by conquering the subproblems subsequently. 

\section{Cross Product}
\begin{wrapfigure}{r}{0.4\textwidth}
	\vspace{-1cm}
	\centering
	\begin{minipage}{0.4\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig/cross_product}
		\caption{Geometrical interpretation of the cross-product in $\mathbb{R}^3$, between the vectors $\vec{u}$ and $\vec{v}$ resulting in the vector $\vec{n}$. Also the relationship between $\vec{n}$ and the parallelogram, spanned by the two operands $\vec{u}$ and $\vec{v}$ is depicted.}
		\label{fig:crossprod}
	\end{minipage}
\end{wrapfigure}
The cross product is defined in $\mathbb{R}^N,\ N \in \{2, 3\}$. In most cases of the robotics context it is further restricted to $N=3$. The cross product is a binary operation and thus has two arguments are $u,\ v \in \mathbb{R}^3$ and is defined as: 
\begin{align*}
	\vec{u}\times\vec{v} &=
	\begin{bmatrix}
	\vec{u}_1\vec{v}_2 - \vec{u}_2\vec{v}_1 \\
	\vec{u}_2\vec{v}_0 - \vec{u}_0\vec{v}_2 \\
	\vec{u}_0\vec{v}_1 - \vec{u}_1\vec{v}_0
	\end{bmatrix}
	= \vec{n} \\
	&\Rightarrow \vec{n}\perp\vec{u} \wedge \vec{n}\perp\vec{v}
\end{align*}
It results in a vector $\vec{n}$, orthogonal to both of its arguments. This property is often used to create orthogonal local coordinate systems. The resulting vector $\vec{n}$ is not only orthogonal, but also points into the direction of a right handed coordinate system, with respect to $\vec{u}$ and $\vec{v}$. This so-called \textit{right-hand-rule} shows that the cross product is not commutative. Changing the operands' order inverses the direction of the resulting vector, i.e. $\vec{x}\times\vec{y}=-(\vec{y}\times\vec{x})$. The operand vectors span a parallelogram, like depicted in Fig.~\ref{fig:crossprod}. It is also shown that the area $A$ of this parallelogram corresponds to the norm of $\vec{n}$. Another relation exists between the angle $\phi$ between $\vec{u}$ and $\vec{v}$, and $||\vec{n}||$. The area of a parallelogram can be formulated using the opening angle between its two neighbored sides and their length. This can be applied to the angle between $\vec{u}$ and $\vec{v}$, and their respective magnitudes.
\begin{equation}
||\vec{u}\times\vec{v}|| = ||\vec{n}|| = ||\vec{u}||*||\vec{v}||*\sin(\phi),\ \ \ \phi = \sphericalangle(\vec{u},\vec{v})
\end{equation}
As $\phi$ approximates to zero or $\pi$, i.e. the operants get parallel or anti-parallel the parallelogram degenerates to a line. This means that $A$ becomes also zero.

\section{Transformations}
A transformation $T$ is a mapping between different vector spaces of arbitrary dimensionality
\begin{equation}
	T(v): \mathbb{R}^N \longmapsto \mathbb{R}^M.
\end{equation}
In the context of mechanics and robotics, the term transformation refers to a subset of this general definition, called \textit{rigid transformation}. If they are used in physical systems or simulations of such, they are defined for vectors in $\mathbb{R}^3$. Moreover, the effects of rigid transformations underlie further restrictions to be able to represent rigid physical systems. They are only allowed to rotate, translate and reflect the input vector, since other operations like scaling or shearing can not happen in this context. These conditions are met for transformations that only rotate an input vector $\vec{x} \in \mathbb{R}^3$. In fact, these operation can be formulated a matrix-vector multiplication.
\begin{equation}
	\vec{y} = R\vec{x},\ \ \ R \in \mathbb{R}^{3\times3} \wedge \vec{x},\vec{y} \in \mathbb{R}^3
\end{equation}
There are three rotation matrices that rotates a vector $\vec{x}$ around a specific axis of its reference frame \cite{BenAri2018ElementsOR}. Each of the rotation matrices $R_x(\alpha), R_y(\beta), R_z(\gamma) \in \mathbb{R}^{3x3}$ is parameterized with a rotation angle. The arguments $\alpha$, $\beta$ and $\gamma$ are often referred to as the Euler angles \textit{roll}, \textit{pitch} and \textit{yaw}. The matrices rotate the input vector around the $x$-, $y$- and $z$-axis, respectively.
\begin{equation}
R_x(\alpha) =  \begin{bmatrix}
1 & 0 			 & 0 \\
0 & \cos(\alpha) & -\sin(\alpha) \\
0 & \sin(\alpha) & \cos(\alpha)
\end{bmatrix} 
\end{equation}

\begin{equation}
R_y(\beta) =  	\begin{bmatrix}
				\cos(\beta)  & 0 & \sin(\beta) \\
				0			 & 1 & 0 \\
				-\sin(\beta) & 0 & \cos(\beta)
				\end{bmatrix} 
\end{equation}

\begin{equation}
R_z(\gamma) =  \begin{bmatrix}
\cos(\gamma) & -\sin(\gamma) & 0 \\
\sin(\gamma) & \cos(\gamma)  & 0 \\
0			 & 0			 & 1
\end{bmatrix} 
\end{equation}
To rotate the input with an single operation the rotation matrices can be multiplied before they are applied to the input vector. 
\begin{align}
\vec{y} &= R_z(\gamma)*R_y(\beta)*R_x(\alpha)*\vec{x} \\
\Leftrightarrow \vec{y} &= R\vec{x},\ \ \ R=R_z(\gamma)*R_y(\beta)*R_x(\alpha)
\end{align}
Note, that matrix multiplications are not commutative and thus the multiplication order needs to be consistent. This properly encodes the rotation of an vector, but can not describe a translation. However, this can easily be done by adding a translation vector $\vec{t} \in \mathbb{R}^3$ to the product
\begin{equation}
	\vec{y} = R\vec{x} + \vec{t}.
\end{equation}
To get a more convenient formulation for such an rigid transformation, the translation can also be written as a matrix multiplication. To do so, the input vector has to be extended by a further dimension. The \textit{homogeneous} coordinate $h = 1$. 
\begin{equation}
	\begin{bmatrix}
	\vec{y}_0 \\
	\vec{y}_1 \\
	\vec{y}_2 \\
	h 
	\end{bmatrix}
	= 
	\begin{bmatrix}
	\vec{x}_0 + \vec{t}_0 \\
	\vec{x}_1 + \vec{t}_1 \\
	\vec{x}_2 + \vec{t}_2 \\
	h 
	\end{bmatrix}
	=
	\begin{bmatrix}
	1 & 0 & 0 & \vec{t}_0 \\
	0 & 1 & 0 & \vec{t}_1 \\
	0 & 0 & 1 & \vec{t}_2 \\
	0 & 0 & 0 & 1 
	\end{bmatrix}
	\begin{bmatrix}
	\vec{x}_0 \\
	\vec{x}_1 \\
	\vec{x}_2 \\
	h 
	\end{bmatrix}
	\label{eq:translmat}
\end{equation}
As indicated in Eq.~\ref{eq:translmat}, the original components of the input vector $\vec{x}$ are added up with the components of the translation vector $\vec{t}$. This sum can also be applied to the rotated vector, including the rotation matrix $R$ in the same rigid transformation matrix $T \in \mathbb{R}^{4\times4}$ as the translation vector
\begin{equation}
T =
\begin{bmatrix}
R & \vec{t} \\
\begin{matrix}
0 & 0 & 0 \\
\end{matrix} & 1
\end{bmatrix},\ \ \  R \in \mathbb{R}^{3x3} \wedge \vec{t} \in \mathbb{R}^3.
\label{eq:transdef}
\end{equation}
In general, the introduction of an homogeneous coordinate requires to scale the resulting vector with $\frac{1}{h}$, in order to ensure $h=1$. This can be ignored in the context of rigid transformation, since they never will affect $h$.

\section{RANSAC}
Random Sample Consensus (RANSAC) is a nondeterministic algorithm to robustly fit a model $f(x) = \mathbb{R}^M \longmapsto \mathbb{R}^N$ to a dataset. It was introduced by Fischler and Bolles \citep{Fischler1981RandomSC}. RANSAC picks a subset $T \subseteqq S$ of data points from a set $S = \{x_0, x_1, \dots\}, x_i \in \mathbb{R}^N$ of noisy data, where the size of $T$ depends on the specific model. A line, for example, takes two different points to be uniquely defined, while a plane requires three non-collinear points. Once the data points are randomly chosen and the model's coefficients $C \in \mathbb{R}^M$ are derived from the elements of $T$, the data points remaining in the original set are tested to be in- or outliers. Points are considered inliers if their distance to the model does not exceed a certain threshold $d \in \mathbb{R}$, and as outliers, otherwise.
\begin{equation}
	I = \{x_i\ |\ min(||f(C) - x_i||) < d\},\ \ \ x_i \in S
\end{equation}
The threshold $d$ is one of two tuning parameters of the RANSAC algorithm. The size of the inlier set $I$ works as a score for the specific parameter set $C$, i.e.\, models producing more inliers are considered to better fit the data. This is repeated several times with different randomly chosen subsets $T$. The number of repetitions $l \in \mathbb{N}$ is the second tuning parameter of the algorithm. After several runs, the coefficient set $C^{\ast}$ resulting in the largest set of inliers is the optimal configuration:
\begin{equation}
	C^{\ast} = argmax(||I(C)||)
\end{equation}
Obviously, RANSAC is not deterministic, as the elements of each $T$ are randomly chosen. However, the probability of getting a good fit increases with the number of runs $l$ with different $T$. In fact, RANSAC was shown to be reliable even on noisy data when enough iterations are performed.

\section{Region Growing}
Region growing refers to a set of algorithms to separate datasets $S = \{x_0, x_1, \dots\},\ x_i \in \mathbb{R}^N,\ i = 0, 1, \dots M$ into segments of neighboring data points. The neighborhood in a dataset can be defined by a structure the data is put in, like a 2D grid in images. However, such a structure is not required, since any binary function can by applied to a pair of two data points $x_i, x_j \in S$ to decide if $x_i$ and $x_j$ are neighbors, or not. 

In many region growing algorithms, the Euclidean distance between two points that does not exceed a certain threshold parameter defines the neighborhood for an unstructured dataset. 

The growing starts with the selection of a seed point $x_s \in S$ from where the segment begins to grow. The seed is initially added to a corresponding set $T_k$ representing the k-th segment. In addition, another set $F$ keeps track of the frontier of the growing region. $F$ is initialized with the neighbors of $x_s$. Afterwards, all frontier points $f_l \in F$ are analyzed. Those $f_l$ meeting the growing condition, e.g.\, having a normal pointing in the same direction as $x_s$' normal, are moved to the segment $T_k$. Only the neighbors of such $f_l$ are added to the frontier $F$. To extract a single connected region, having similar properties, this is repeated until $F$ is empty. To obtain further segments this procedure has to be done with an updated $S = S \setminus T_k$, i.e.\ without the already segmented points. 

There might be further parameters, like the allowed normal deviation, for specific region growing algorithms to define similar properties.






%\section{Singular Value Decomposition}