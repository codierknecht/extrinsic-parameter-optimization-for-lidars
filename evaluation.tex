\chapter{Evaluation} \label{chap:evaluation}
\begin{wrapfigure}{r}{0.55\textwidth}
	\centering
	\begin{minipage}{0.55\textwidth}
		\includegraphics[width=\textwidth]{fig/arrangement}
	\end{minipage}
	\caption{Two possible target arrangements. The left depicts an optimal placement with high target distances $L_{02}$ and $L_{13}$ as wall as an angle between them of $\omega = \ang{90}$. The arrangement on the right, however the targets close to the LiDAR and an angle $\omega' \neq \ang{90}$ between ${L'}_{02}$ and ${L'}_{13}$. The latter is expected to lead to suboptimal calibration results.}
	\label{fig:arrangement}
\end{wrapfigure}
The optimization quality strongly depends on the arrangement of the calibration targets. There are two quantities that probably affect the calibration result: (1) is the distance between the targets and the LiDAR and (2) the angle between the direct lines corresponding to the pairs of spheres.

The distances $||L_{02}||$, $||L_{13}||$, $||{L'}_{02}||$ and $||{L'}_{13}||$ in Fig.~\ref{fig:arrangement} have a direct influence on the \textit{sensitivity} of the objective function for the orientation optimization. The sensitivity of a function is the magnitude of the change of its response when its arguments are changed, i.e.\, how steep the gradient is, relative to the change in a certain argument. Thus, the sensitivity has an influence on the optimization behavior and should be high. For the distance this means that the sensitivity with respect to $\phi$ is of interest:
\begin{align}
E_{ij}(R) &= ||\vec{d_i}\times(-\vec{d_j})|| \\
&= ||\vec{d_i}||*||-\vec{d_j}||*\sin(\phi),\ \phi = \sphericalangle(\vec{d_i},(-\vec{d_j})) \\
\frac{dE_{ij}(R)}{d\phi} &=  ||\vec{d_i}||*||-\vec{d_j}||*cos(\phi). \label{eq:derivative}
\end{align}
The derivative, with respect to the angle $\phi$, determining the area of the spanned parallelogram, as described in Sec.~\ref{sec:optimization}, is scaled by the squared distance between the spheres of a pair. Therefor, we would expect, that a larger distance between the platform and the targets is beneficial for the optimization results.

The influence of the angles $\omega$ and $\omega'$, however, does not directly affect the objective function. However, it affects the sensitivity of the coupling between two target pairs. If a change of the extrinsic parameters of the LiDAR results in a rotation of angle $\alpha$ around the direct line $L_{ij}$ between a certain pair of spheres, this does not change the residual contributed to the overall error, by this pair. Moreover, this rotation would increase the residual contributed by the other pairs. How strong this contribution is, is defined by the orthogonal portion $r_{kl}=d_{kl}\sin(\omega)$, from one direct line $L_{ij}$ to an other $L_{kl}$. The constant $d_{kl} = ||L_{kl}||$ refers to the distance between a sphere and the platform. Rotating around $L_{ij}$ would displace the spheres centers defining $L_{kl}$. The magnitude of the displacement can be described as an chord of length $l$:
\begin{align}
	l &= 2 r_{kl} \sin(\alpha) \\
	  &= 2 (d_{kl}\sin(\omega)) \sin(\alpha) \\
	\frac{dl}{d\alpha} &= 2 (d\sin(\omega)) \cos(\alpha) \label{eq:magindirect}
\end{align}
The derivative, with respect to $\alpha$ in Eq.~\ref{eq:magindirect} suggests that the coupling of two target pairs is more sensitive if $\omega \approx \pi$. Therefore, better optimization results are expected if the direct lines between the pairs of spheres are orthogonal.

Further investigations about this expectations will be done in Sec.~\ref{sec:experiments} and Sec.~\ref{sec:discussion}. The experiments using the sensor described in Sec.~\ref{sec:platform}, will assess the calibration qualities of different target arrangements, by applying the quality measures described in Sec.~\ref{sec:quality}.

\section{SWAP Platform} \label{sec:platform}
The platform used for our experiments was introduced by Tobias Neumann et al.\ in \citep{SWAP}. As can be seen in Fig.~\ref{fig:swap}, the \textit{Rotating Platform for Swift Acquisition of Dense 3D Point Clouds} (SWAP) is a construction with a rotating carrier on top. The actual orientation of this carrier is determined by an absolute encoder with an angular resolution of \ang{0.022}. There are two LiDARs mounted on the platform. 

One is a \textit{Hokuyo UTM-30LX-EW} 2D LiDAR for high resolution, close range perception for distances of up to 30m with low noise. It is mounted with its scanning plane vertical to the ground and perpendicular to its rotation trajectory. In this way its measurements can be transformed to 3D points to create a nearly full spherical field of view, when the carrier is rotating. 
\begin{wrapfigure}{r}{0.6\textwidth}
	\centering
	\begin{minipage}{0.6\textwidth}
		\includegraphics[width=\textwidth]{fig/swap}
	\end{minipage}
	\caption{Schematic side-view \citep{SWAP} of the SWAP platform construction, showing the most important parts used for calibration. The red rectangle encloses the whole rotating part of the sensor platform. Mounted on the carrier are the Velodyne VLP-16 multi-beam LiDAR (green rectangle) which is calibrated and a Hokuyo UTM-30LX-EW, seen in a top-view and enclosed by the yellow circle. The blue rectangle contains the encoder measuring the carrier orientation referred to as $R_i$. It is mounted on the rigid part of the platform.}
	\label{fig:swap}
\end{wrapfigure}

The other one is a \textit{Velodyne VLP-16} 3D LiDAR with 16 scanlines, distributed on a \ang{30} aperture angle. In contrast to the 2D LiDAR, it is used for the sampling of surfaces between 0.9m and 100m away from the platform. However, this far view comes with a significantly higher noise. It is also tilted about \ang{76}, relative to the carrier's x-y-plane. As described in \citep{SWAP}, the tilt is done to draw more uniformly distributed points in the accumulated point cloud. The Velodyne has a \ang{360} azimuth angle. Its center scanning plane is parallel to its orbital track. The orbital track results from the fact that both sensors are shifted away from the rotating carrier's center point. Thus, they are revolving, i.e.\, rotating around an other center then their own. 

The points measured by the two sensors have to be transformed and accumulated constantly from their local frame to the rigid platform base while the carrier is rotating. To do this, the poses have to be known precisely, however, an exact pose is hard to measure is hard to measure, e.g.\, because the Velodyne VLP-16 has a cylindric shape and thus the rotation around its up-direction is not easy to determine. The following sections will estimate the extrinsic parameters of the mounted Velodyne VLP-16 3D LiDAR with different sphere arrangements and measure the improvement of the point cloud qualities.

% NOTE: here is a manual pagebrake
\newpage

\section{Quality Measure} \label{sec:quality}
To compare the point clouds $X_k = \{\vec{x}_0, \vec{x}_1, ... \vec{x}_{N-1}\},\ \vec{x}_i \in \mathbb{R}^{3\times1}$ resulting from calibration runs with different target arrangements, a quality measure called \textit{crispness} introduced by Maddern et al.\ in \citep{maddern2012lost} will be briefly discussed. Moreover, a further measure will be introduced, which is more specific to the environment during our experiments. 

The crispness measure was introduced as a quality measure constructed from a Gaussian mixture model (GMM) and an entropy function applied to the GMM. The crispness represents how \textit{sharp} a point cloud is, i.e.\, it is low if there is a high deviation of the samples to from the underlying surface. The sum of Gaussians draws a spatial distribution in the neighborhood of a probe $\vec{x}$:
\begin{align}
p(\vec{x}) &= \frac{1}{N} \sum_{i=0}^{N} G(\vec{x}-\vec{x}_i, \Sigma_i + \sigma^2I) \label{eq:gmm} \\
G(\vec{x}-\vec{\mu}, \Sigma) &= \frac{1.0}{(2*\pi)^{3/2}*det(\Sigma)}*e^{-\frac{1}{2}*(\vec{x}-\vec{\mu})^T*\Sigma^{-1}*(\vec{x}-\vec{\mu})},
\end{align}
\begin{equation*}
	\vec{x}, \vec{x}_i, \vec{\mu} \in \mathbb{R}^{3\times1} \wedge \Sigma, \Sigma_i \in \mathbb{R}^{3\times3}
\end{equation*} 
As can be seen in Eq.~\ref{eq:gmm}, the covariance matrix $\Sigma_i$ associated with the probe $\vec{x}_i$ is added to the isotropic Gaussian kernel. $\Sigma_i$ comes from the localization uncertainty of the moving sensor platform at the moment $\vec{x}_i$ was recorded \textendash{} the original \citep{maddern2012lost} considers a mobile sensing platform mounted on an actual car. However, to get a score that measures the whole point cloud, Maddern et al.\ propose to apply the Rényi Quadratic Entropy \citep{renyi1961measures} to their GMM:
\begin{equation}
H_{REQ}(X) = -\log \int p(\vec{x}_i)^2 dx. \label{eq:req}
\end{equation}
A discretization of Eq.~\ref{eq:req}, applied to every pair of points $\vec{x}_i, \vec{x}_j \in X$ leads to
\begin{equation}
H'_{REQ}(X) = -log \left( \frac{1}{N^2} \sum_{i=0}^{N} \sum_{j=0}^{N} G(\vec{x}_i-\vec{x}_j, \Sigma_i+\Sigma_j+\sigma^2I) \right)
\end{equation}
Moreove, further simplifications are proposed to get rid of monotonic parts in the equation:
\begin{equation}
E(X) = \sum_{i=0}^{N} \sum_{j=0}^{N} G(\vec{x}_i-\vec{x}_j, \Sigma_i+\Sigma_j+\sigma^2I) \label{eq:req_simple}
\end{equation}
Eq.~\ref{eq:req_simple} has removed the scaling factor $\frac{1}{N^2}$ and the logarithm, since both monotonic scalings do not change the minimization behavior, which is the original purpose. In addition, this simplifies the function and has no effect on the expressiveness of the crispness measure, so it is also adopted in our experiments. Maddern et al.\ also describe further computational improvements. Instead of summing up the Gaussians of all point pairs in the whole point cloud, they argue that far points have no significant effect on the total sum and thus can be ignored. To do so, they define a radius $r_i$ around $\vec{x}_i$ based on the biggest eigenvalue $\lambda_0$ of its respective covariance matrix:
\begin{equation}
	r_i = 2k(\lambda_0(\Sigma_i)+\sigma^2).
\end{equation}
Here, $k$ represents a regulation parameter to adjust the radius without affecting the results of the Gaussian function. This is because in the absence of the uncertainty information provided by a localization system or similar, the algorithm was adapted to apply the crispness also to stationarily recorded point clouds. Here, the covariance matrices, needed for constructing the kernel and calculating a suitable radius for the probe's neighborhood, are locally estimated. The region around $\vec{x}_i$ in which the covariance is estimated should depend on the used $\sigma$ and $k$. Since $\Sigma_i$ describes the expected changes around $\vec{x}_i$, the GMM gets more peaky if many $\vec{x}_j$ meet this expectation. The crispness of a dataset is then formulated as
\begin{equation}
	E(X) = \sum_{i=0}^{N} \sum_{\vec{x}_j \in M} G(\vec{x}_i-\vec{x}_j, \Sigma_i+\Sigma_j+\sigma^2I), \ \ \ M = \{\vec{x}_j \mid\  ||\vec{x}_i-\vec{x}_i|| \leq r_i \}. \label{eq:req_simple_enhanced}
\end{equation}

Since this measure dose not take any special structures in the environment into account, it is generally applicable, but the crispness describes the point cloud properties in an very abstract way. Thus, we propose another measure, that we also use in our experiments. Since the experiments are done in an artificial environment, there are many planar surfaces in the scene. How crisp or sharp a point cloud is, can therefore be measured as how noisily those planar regions have been sampled. Therefore, the largest planes are subsequently detected by fitting planes to the point cloud via a \textit{RANSAC} method followed by a region growing taking only the Euclidean point distance into account, to separate planar unconnected patches lying in the same plane, such as several tables of the same height. This results in a set of smaller point clouds $P_k = \{\vec{p}_0, \vec{p}_1, ... \vec{p}_{N_k-1}\},\ k=0, 1, 2, \dots, M-1$, each containing the samples of the respective planar surface patch. Afterwards, the centroid $\vec{\mu}_k$ of each patch is calculated as
\begin{equation}
	\vec{\mu}_k = \frac{1}{N_k} \sum_{i=0}^{N_k-1} \vec{p}_i,\ \ \ \vec{p}_i \in \mathbb{R}^{3\times1}, i=0, 1, 2, \dots, N_k-1
\end{equation}
and used to compute the covariance matrix $C_k$ of this point set $P_k$ as
\begin{equation}
	C_k = \frac{1}{N_k} \sum_{i = 0}^{N_k-1} (\vec{\mu}_k-\vec{p}_i)^T*(\vec{\mu}_k-\vec{p}_i),\ \ \ \vec{\mu}_k, \vec{p}_i \in \mathbb{R}^{3\times1}, i=0, 1, 2, \dots, N_k-1.
\end{equation}
The covariance matrix $C_k$ encodes the squared deviations in the dataset with respect to $\vec{\mu}_k$. Since, the data points have three dimensions, $C_k$ also has three eigenvectors $\vec{v}_{k0}, \vec{v}_{k1}, \vec{v}_{k2}  \in \mathbb{C}^{3\times1}$, with
\begin{equation}
C_k*\vec{v_{ki}} = \lambda_{ki}*\vec{v_{ki}},\ \ \ i = 0, 1, 2.
\end{equation}
where the eigenvalues $\lambda_{k0}, \lambda_{k1}, \lambda_{k2} \in \mathbb{C}$ with $\lambda_{k0} \geq \lambda_{k1} \geq \lambda_{k2}$ associated with the eigenvectors $\vec{v}_{k0}, \vec{v}_{k1}, \vec{v}_{k2}$, and represent how strong the deviation in the respective direction is. Because a plane only expands in two dimensions, the eigenvector $\vec{v}_{k2}$, associated with the smallest eigenvalue $\lambda_{k2}$, is perpendicular to the plane, if the sampled data contains no additional systematic noise in other directions. In fact, the magnitude of change in this direction has to be the sensor noise or systematic deviations like they appears when points are erroneously transformed from sensor- to common frame (Fig.~\ref{fig:errorexample}). Summing up this error for every plane $P_k$ found in the scene and normalizing it with the number of planes performs as a measure $E(P)$ for the cloud sharpness:
\begin{equation}
	E(P) = \frac{1}{M} \sum_{k=0}^{M-1} \lambda_{k2}.
\end{equation}


\section{Experiments} \label{sec:experiments}
The error caused by mistransformation in a point cloud is difficult to isolate, since there are different possible sources for errors. For example, there are errors caused by LiDAR noise or by caused the discrete measurement of the rotation encoder, resulting from angles which are actually between two encoder steps. Neither the source nor the respective portion of the sensory error can be distinguished from the residual error after calibration. Furthermore, a further problem arises even another problem preventing the direct analysis of the actual remaining transformation error. The data used during the calibration process are estimated sphere centers. These estimations are themselves erroneous and prevent the calculating the exact pose. Therefore, the quality measures described in Sec.~\ref{sec:quality} are applied to the uncalibrated LiDAR measurements and to all calibration results with the different target arrangements, parameter grid resolutions, and initial pose guesses. Rating the outcomes of the calibration in different conditions allows a reasoning about the method's behavior.

For the experiments, the platform was placed in the hall of a building on our campus, i.e.\, in an environment with many planar surfaces, that are not too close to the platform (ca.\ $5\si{m}$ away). This is important, since errors caused by a orientation of the LiDAR increase with the distance of the sampled surface data. The experiments can be split into two phases. The first phase records the calibration targets with in the different arrangements discussed earlier and calibrates the extrinsic parameters using different configuration sets. The second phase takes the resulting LiDAR poses from the first phase, applies them to the platform configuration to record the environment, i.e.\, the building on our campus, in operation-mode. Finally the proposed quality measures are applied to the record to compare the several calibration results.

In the first phase, the spherical calibration targets were placed around the LiDAR platform and were recorded in calibration mode. This means that the platform drives to several previously configured angles in sequent, from which the spheres can be scanned by the mounted Velodyne VLP-16 LiDAR to estimate their center points. These scans were recorded and passed to the calibration software, which optimizes the kinetic chain as explained earlier. We performed several calibration runs under different circumstances. First, the target arrangements were changed, i.e.\, the targets were placed at different distances of  $2.0\si{m}$, $3.0\si{m}$, $3.5\si{m}$ and $4.0\si{m}$ from the sensor platform. This allows us to estimate how the sensitivity changes with the distance, as mentioned in Sec.~\ref{sec:optimization}. Furthermore, we in addition, expected, that it is more beneficial to have a \ang{90} angle between the $\vec{d}$-pairs. Therefore, the target pairs were additionally placed in a $\omega\approx\ang{90}$ and a $\omega'\approx\ang{45}$ angle from each other (see Fig.~\ref{fig:arrangement}). In addition to the physical variations during the experiments, also calibration parameters were changed. First, is the resolution of the search grid, used to find the global optimal orientation parameters, was configured to a $0.005$ step size in each dimension, to test a low resolution grid. To study a significantly higher resolution, the step size was also set to $0.00025$ in yaw, pitch and roll. Second, to investigate how the method behaves if the initial pose guess is far from the true pose, the actually measured pose was tested in comparison with one with an additional error of $+0.03$, added to all five dimensions.

The calibration runs result in different transformations that were used to record the surroundings in a second phase. While running the platform in operation mode, it was configured to rotate continuously with $0.0175\si{Hz}$ (i.e.\, one full rotation in about a minute) to sample the environment. The records of the continuous runs were cropped to an excerpt of the room to avoid negative influence of clutter or salient structures of the building. The plane deviation and crispness measure were applied to each of these records to rate the quality of the respective point cloud. We set the parameters of the measures as followed. The $\sigma$ for the crispness measure was set to the sensor's noise of $0.03\si{m}$, as suggested in the original paper by Maddern et al.\ \citep{maddern2012lost}. Setting $k=1.5$ turned out to be suitable during the experiments, i.e.\, a $k < 1.5$ influences the results of different runs unequally, while a $k > 1.5$ only scales the crispness results. The RANSAC of the plane deviation has a parameter to set the allowed distance between the plane model and the data points to distinct plane inlier from other points. This was set to $2\sigma = 0.06$, to prevent the potential oversegmentation of calibration results with low quality, i.e. high noise and thus also high point deviations. The same applies for the allowed point distance during the region growing.

The qualities of the recorded point clouds are plotted against the different target distances in Fig.~\ref{fig:pcqualities}.

% The ones which does not can not finally be explained at the moment and maybe require further research.
	
\section{Discussion} \label{sec:discussion}
\begin{figure}
	\centering
	\begin{minipage}{\textwidth}
		\includegraphics[width=0.5\textwidth]{image/plane_meas_plot}
		\includegraphics[width=0.5\textwidth]{image/plane_det_plot}
		\includegraphics[width=0.5\textwidth]{image/crisp_meas_plot}
		\includegraphics[width=0.5\textwidth]{image/crisp_det_plot}
	\end{minipage}
	\caption{Results of experiments calibrating the LiDARs extrinsic parameters under different circumstances and with different quality measures. The graphs in the left column show the quality of recorded point clouds after calibration from a pose we actually measured at the platform construction, rated with the \textit{plane deviation} (above) and the \textit{crispness measure} (below). The right column depict the same ratings but applied to results from an initial pose guess with an artificially upscaled error. All graphs contain ratings of calibration results produced from different target arrangements, each calibrated with a high ($0.00025$) and a relatively low ($0.005$) resolution of the pose parameter grid.}
	\label{fig:pcqualities}
\end{figure}
The plots in Fig.~\ref{fig:pcqualities} indicate an improvement of the extrinsic parameters compared to the respective initial guess, as measured with the plane deviation. Most of these results are supported by the crispness measure. The tendency to the optimal configuration supported by both quality measures goes to a sweet-spot at a distance between sensor and targets of $3.5\si{m}$ and a high resolution for the grid search (blue line and red dotted line). This is indicated by a very low plane deviation and a high crispness for these configurations. Other configurations with close targets or low resolution of the search grid also improve the initial pose, bus score less compared to the sweet-spot. 

%-- distance has to be large
It is significant, that targets placed at great distances lead to better results, at least for the promising configurations. The score gets better up to $3.5m$. Calibration with targets placed in $4.0\si{m}$ range come up with worse scores. We believe that this is caused by the increasing error from sphere fitting, dominating the benefit of targets placed farther away. In general calibration runs with targets placed close to the platform result in less sharp point clouds. As predicted by our expectations, this can possibly be explained via the sensitivity of the objective function to optimize (see Sec.~\ref{sec:optimization}) and with the fact that a divergent angle has less effect at close range. This could possibly shift or widen minima to seddles the optimizer converges to. This makes it necessary to place the targets as far as possible from the platform as long as an accurate sphere center estimation can be expected.

%-- high res necessary
It was expected to get better results using the higher resolution for the parameter grid used for the search of promising points to start a gradient decent optimization at. In fact, the results contain no evidence for a good result on low resolved search grids. Thus, a high resolution search seems to be not only beneficial, but also necessary. 

%-- angle has no effect
Surprisingly, the angle between the target pairs (see Fig.~\ref{fig:arrangement}) had no strong or even any affect on the results. The fact that the \ang{45} arrangement scores even the best can arguably explained by an inferior estimation of sphere centers when the  data for the \ang{90} arrangement were recorded. There is no theoretical reason for the acute-angle to be an advantage.

\begin{figure}
	\centering
	\begin{minipage}{\textwidth}
		\includegraphics[width=0.5\textwidth]{image/measurement_init}
		\includegraphics[width=0.5\textwidth]{image/artificial_init}
		\includegraphics[width=0.5\textwidth]{image/measurement_best}
		\includegraphics[width=0.5\textwidth]{image/artificial_best}
	\end{minipage}
	\caption{Point clouds sampling the test scene, especially the excerpt the quality measures were applied to. The images in the upper row show the two initial extrinsic parameters used to study the influence of the initial pose the optimizer starts with. The cloud on the left results from using the sensor pose parameters measured on the SWAP platform by hand. The right one results from applying the transformation resulting from a deterioration of the measured transformation. The bottom row depicts the resulting clouds after parameter optimization of the respective initial pose from the upper row.}
	\label{fig:resultclouds}
\end{figure}
 
At least the plane deviation measure gives evidence that the initially guessed sensor pose does not need to be very accurate. The deteriorated initial pose parameters, shown in the upper right plot of Fig.~\ref{fig:pcqualities}, result in similar qualities as the parameters from the originally measured initial pose (plots in the left column). However, in contrast to the crispness measure for the results starting from the degraded pose, Fig.~\ref{fig:resultclouds} supports this observation. The figure shows the point clouds recorded with the initial pose guesses in the first row, on the left side with the measured and on the right side with the deteriorated pose. The results with the best quality scores (at least suggested by average plane deviation) are shown below their respective initial guess. Both present fairly sharp point clouds. It is not clear to us why the crispness measure leads to such significantly different scores in this case.

