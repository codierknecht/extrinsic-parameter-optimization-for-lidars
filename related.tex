\chapter{Related Work} \label{chap:related}

There are many researches out there working on the extrinsic calibration between different sensors. Especially the pose of an LiDAR relative to an camera is very popular. However, this and other works concern the extrinsic calibration of single sensors with respect to the construction they are mounted on. In general these can be distinguished into two types of calibration methods, both having their own benefits and drawbacks. The first type requires previously known calibration targets, such as poles, spheres or planes inside the field of view of the used sensors. Moreover, it is often undesirable to use such objects, as this increases the required configuration effort. Therefore, a second, target-less type of calibration methods was developed. They use static environments and properties of their surroundings like big planar surfaces, which however, are not always available. 

%
%
% target required ---
\section{Calibration Target Required}
A representative of the first method type is described in 
\cite{Gao2010OnlineCO}. It estimates the extrinsic parameters of a LiDAR 
scanner with respect to another LiDAR's frame. Both are mounted on the top of the same car. 
Highly reflective patches are attached to pillars at the edge of a 
street track. While driving along this calibration track, the patches are 
detected by the reflection intensity of their LiDAR measurements and then 
reduced to a single feature point in both sensor frames. Using the known 
vehicle pose in a global reference frame, the error between patches from both 
sensors in a common frame is minimized. This is done by solving a second-order 
cone program which reformulates a more cost effective least-square problem. \\
While the problem formulation is interesting, due to the reduction of computational costs, the proposed method requires a positioning system, and driving along a previously manipulated track. Both is not always feasible and maybe error prone. Furthermore, the calibration results in the knowledge of a sensor's pose with respect to another one's frame and not with respect to a part of the platform construction. This makes knowledge of the exact pose of the other LiDAR, relative to the car necessary.

In \citep{lin2013calibration}, a simple, analytical method is proposed, in contrast to more common numerical minimization algorithms like in \cite{Gao2010OnlineCO}. The 
platform's architecture obtains 3D point clouds from a 2D Laser Range Finder by
tilting it to different angles. Since the rotation center differs from the 
sensor center, it is important to know the radius exactly. Chien-Chou Lin et 
al.\ \citep{lin2013calibration} proposes to measure points on a planar target, e.g. a wall, in different known tilt angles of the sensor. Using the vertical distances of those points, the radius can be inferred using angle and distance information in a triangle spanned by the rotation center and two of the LiDAR measurements. \\
This method ignores all other dimensions of the sensor pose except the radius of the revolving LiDAR. However, these are important for many platforms and thus the proposed method is strongly bound to this specific architecture.

%\citep{Choi2014ExtrinsicCO}
%\begin{itemize}
%\item two orthonogal planes
%\item scanlines of $L_1$ and $L_2$ on each plane
%\end{itemize}

The method proposed in \citep{Kang2016FullDOFCO} uses a target plane to 
estimate the pose of a 2D Laser Range Finder orbiting a rotation center. The 
3D scanline direction on the target is inferred to construct a cost function 
which scores the scanner's rotations separately from its translation, 
which is optimized by a second cost function. These are used as objective to 
minimize the distance between points on neighboring scanlines of different 
platform orientations. The decoupling removes some ambiguities between 
translation and rotation of the scanner's frame. Thus, rotations which compensate translations are removed from the search space of the objective functions. \\
The decoupling is a clever approach. Not only to remove the ambiguities, but also to split up the optimization problem into two functions with lower dimensionality. Some of our main ideas are based on this work. However, it is expected to depend on the device architecture of the LRF used on this platform. 

%
%
% targetless ---
\section{Targetless Calibration}
The proposed method in \citep{Levinson2010UnsupervisedCF} calibrates three 
sensor properties without any calibration targets: intrinsic LiDAR parameters, 
i.e.,\ the internal laser arrangement, external LiDAR parameters specifying its 
pose relative to the robots base frame. To determine the internal and external parameters a single energy 
function is formulated. This function rewards points of neighboring laser beams 
if they lie on the same surface, i.e.\, having the same normal, while applying a penalty, if they 
are not. During a movement of the vehicle with tracked trajectory the measured 
points can be transformed into a common frame. The external parameters 
minimizing the energy function should be the true scanner pose. To avoid 
ambiguities and local minima, Levinson et al.\ \citep{Levinson2010UnsupervisedCF} use a grid search instead of 
a Newton-based method though the energy function would be contentious. They 
state that the search space is tractable. \\
They also state to not make strong assumptions about their environment. However, 
the error formulation of their objective function implies planar surfaces, 
which is a relatively strong assumption when, e.g.\, thinking about urban rescue, 
forest, or underground environments. In addition, the requirement of a local
positioning system is not always practical.

Sheehan et al.\ \citep{Sheehan2010AutomaticSO} constructed a sensor platform 
assembled from three independent 2D Laser Range Finders mounted on a rotating 
plate. To calibrate the extrinsic parameters of all three scanners, they 
formulated a \textit{crispness} function which represents the sharpness of the 
resulting 3D point cloud, i.e.\, the neighborhood distribution of the measured 
points. This measure consists of a Gaussian mixture model (GMM) of each individual point's neighborhood. The GMM gets more 
peaky as the deviation of nearby points in the neighborhood of a point decreases, which happens if the 
transformation to the scanners approximate towards the true ones. To quantify this measure 
in a score that then can be maximized, the model is put into the Rényi Quadratic 
Entropy function \citep{renyi1961measures}. The scanner transformations maximizing the model's 
entropy should be the sought ones. \\
Sheehan's calibration method is tailored to their platform and only calibrates the relative position of the 
sensors to each other.

Maddern et al.\ \citep{maddern2012lost} extend Sheehan's method by the
calibration of several other LiDAR or LRF sensors mounted on a vehicle. They
require to drive a path, so that all sensors see as much of the environment as
possible. Utilizing a positioning system with known covariance matrix, i.e.\, 
known measurement uncertainty, the sensor data can be transformed into a common 
coordinate system. This allows a comparison between the sensor data of the different sensors, especially 
between Sheehan's platform and the other sensor data. To do so, Maddern et al.\ introduced 
another entropy based cross-sensor similarity measure. It integrates the 
Jensen-Rényi Divergence \citep{Wang2009ClosedFormJD} in the crispness function 
of \citep{Sheehan2010AutomaticSO}. The new measure will reach its maximum with a 
transformation that results in the most similar sensor measures in the common frame. \\
The requirement of driving and a working positioning system makes this method 
inapplicable in many situations. The necessary conditions prevail the improvements made compared to Sheehan's method. 

While driving along a V-shaped track, the point clouds recorded by the LiDARs 
mounted on a vehicle should be aligned when they are transformed using the 
correct transformation $M_L$ between LiDARs and vehicle frame. To measure the 
alignment, He et al.\ \citep{He2014CalibrationMF} uses the definition of a fitted plane and the pairwise distance between those planes, introduced in  \citep{He2013PairwiseLC}. This allows He et al.\ \citep{He2013PairwiseLC} to formulate an error function depending on trajectory and LiDAR pose to transform the plane representations into a common frame and minimize its distance. Once a reference LiDAR is calibrated, other mounted LiDARs can be calibrated to minimize the distance between their fitted planes and the reference planes of the already calibrated sensor. \\
Again, driving is required and the use of 2D-SLAM could cause additional transformation errors. It also requires extra hardware. Furthermore, the error based on the distance between the fitted planes is a good error measure, but assumes relatively large planar surfaces in the environment.

In \citep{Oberlnder2015FastCO} Oberl{\"a}nder et al.\ utilize the crispness
introduced in \citep{Sheehan2010AutomaticSO}, and also the redundancy in the observation of a rotating or swiveling 2D Laser Range Finder, mounted on their sensor platform, after a full rotation of this same LRF. 
Given the case that the surrounding environment is static, the scanner has seen 
everything twice after a full revolution. Based on this fact, an objective 
function that represents the crispness of point clouds with this redundancy, 
feature is formulated. To exploit this redundancy the point cloud is separated 
into the two similar parts after a whole rotation. The objective function 
transforms the sample points into a common frame. In difference to \citep{Sheehan2010AutomaticSO}, the 
GMM of each point's neighborhood inside the other redundant point set, 
respectively, is calculated. The quantification is the same as in \citep{Sheehan2010AutomaticSO}. A high crispness, depending on the 6-DOF pose parameters of the scanner encodes a low deviation of points in their respective neighborhood, which in return means, that point pairs in the redundant data are close together, or, in other words represent the same surface patch. In both methods, \citep{Sheehan2010AutomaticSO} and \citep{Oberlnder2015FastCO}, the crispness is optimized using iterative, numerical Newton-like algorithms. \\
However, there emerge ambiguities between translation and rotation when 
the sensors are rotated eccentrically on the platform, i.e.\, rotated around the 
center with a radius $r > 0$. Furthermore, there are problems caused by different view 
angles of the redundant LiDAR measures. Nevertheless, the exploitation of 
redundancies in general is an elegant way to determine the scanner's pose.

The method proposed in \citep{nouira2015target} makes use of the precise knowledge of the vehicle's pose. They state, that 
sample points on a planar surface of the same or a neighboring scanline should 
be close together, if the transformations from navigation frame to the vehicle's 
baselink and finally to the scanner's frame are known and correct. Due to the fact that the 
pose is known by several sensors, Nouira et al.\ \citep{nouira2015target} were able to formulate an objective 
function which minimizes the Euclidean distance of neighboring points on 
planar surfaces. \\
This, however, requires several additional sensors and driving during the
calibration process. Moreover, some of the required sensors, such as GPS are not always available. Besides the sensor requirements there are difficulties exploiting the environments structure to avoid calibration targets. Planar surfaces are mostly found in artificial environments.

Some of the proposed methods require driving. That makes them inapplicable in 
many situations, for example, in stand-alone operation of sensing platforms. 
Others require additional localization information which are also often not 
available. Avoiding the use of calibration targets comes with strong 
assumptions about the agent's environment. This will make recalibration 
sometimes difficult. Our novel calibration method does not require any 
additional localization nor makes it any assumptions about the scanner's 
surroundings during the calibration process. Moreover, the majority of methods 
either optimize the sensor pose information with respect to other sensors or to 
other the vehicle's base link. The usage of redundancies in the sensor 
perception allows our calibration method to calibrate both, as the redundant 
information can be replaced by measurements of an already calibrated LiDAR.