\chapter{Calibration Method} \label{chap:calibration}

\begin{wrapfigure}{l}{0.33\textwidth}
	\centering
	\begin{minipage}{0.33\textwidth}
		\includegraphics[width=\textwidth]{fig/redundency}
		\caption{Perception redundancy after a full carrier rotation. The triangles in blue and red show the LiDAR's frustums in a simplified manner. The lines, also in blue and red are the measurement in the respective view direction. The figure shows four representative carrier orientations $R_i \in \mathbb{R}^{3x3},\ i = 0, 1, 2, 3$ with the revolving LiDAR and how its perceptions construct a full scan after a \ang{180} carrier rotation.}
		\label{fig:redundency}
	\end{minipage}
\end{wrapfigure}
On many sensing platforms, LiDARs are mounted on a rotating carrier, to enable the sensors to extend the field of view. Thus, it changes its direction while continuously scanning the environment in operation mode. The steady change of view direction makes a correct transformation of the drawn sample points more important, because errors do not only result in a static rotated or shifted perception, but in many accumulated errors of this kind. The necessary transformation corresponds directly with the sensor's pose with respect to the sensor carrier's rotation center, which is detailed in Sec.~\ref{sec:kinematic}. 

The LiDAR is mounted in a way that it's frustum is either tangential to the trajectory of the scanner rotation, like indicated by the colored triangles in Fig.~\ref{fig:redundency} or the frustum is intersecting this trajectory, for example when the scanner in Fig.~\ref{fig:redundency} is rotated by \ang{90}. However, in both cases, there is a redundancy in the measurements done by the LiDAR after a full carrier rotation. The reason for this is that, at each carrier orientation $R_i \in \mathbb{R}^{3x3}$, the LiDAR scans in opposite directions, simultaneously. These scans are depicted as blue and red lines in Fig.~\ref{fig:redundency}. It also shows that a full perception of the platform's surroundings is already completed after a \ang{180} carrier rotation. Thus, there are two full, separable scans recorded after a \ang{360} turn. However, if the transformation of the measured surface samples from the sensor's frame into a common one is not configured with the true LiDAR pose, the two resulting, redundant point clouds will differ. The magnitude of this divergence directly corresponds to the difference between the configured extrinsic parameters of the LiDAR and its real-world pose.\\
\begin{figure}
	\begin{minipage}{\textwidth}
		\centering
		\includegraphics[width=0.49\textwidth]{fig/sphere_0}
		\includegraphics[width=0.49\textwidth]{fig/sphere_1} \\
		\includegraphics[width=0.49\textwidth]{fig/sphere_2}
		\includegraphics[width=0.49\textwidth]{fig/sphere_3}
		\caption{Sphere center points around the revolving LiDAR in the middle of the images. As in Fig.~\ref{fig:redundency}, the spheres are colored and denoted with $p_i,\ q_i,\ i=0,1,2,3$ according to their respective frustum-direction and the carrier's i-th orientation $R_i$. From top-left to bottom-right, the sphere centers are estimated by the LiDAR measurements. The first picture most clearly reveals the fact that two spheres can be seen by the LiDAR without further movement. The second picture shows the scanner observing the second pair of spheres, after the carrier has turned by \ang{90}. In the third image, the first target pair is detected a second time after another \ang{90} rotation. Note, the different colors indicate that it is seen from a different carrier orientation. Furthermore, note that, the center points are not aligned. This results from the actual transformation error between the sensor frame and the platforms base frame. Moreover, it clarifies that a redundant measurement of the surroundings begins after a \ang{180} carrier rotation. Like the previous one, the last picture shows the other redundant sphere detection after the carrier turns further \ang{90}. In each picture's center area, the revolving LiDAR is depicted in the coordinate system of the rotating sensor carrier. As can be seen, the LiDAR's pose with respect to the carrier is static, i.e-\, it does not change over time.}
		\label{fig:spheredet}
	\end{minipage}
\end{figure}
Inspired by Oberl{\"a}nder's approach \citep{Oberlnder2015FastCO}, this is a key observation for the proposed method. The objective is to equalize the redundant measurements carried out by the sensor. This can, in principle, be achieved independently from any further tools, only by scanning the environment. However, some experiments with our sensing platform suggested that there are problems with different view points when inferring the LiDAR's pose only from its bare measurements. This results from the fact that rotations with a positive radius, i.e.\, revolving or orbiting a rotation center, may measure the surroundings twice, like explained above. However, the two scans will differ slightly, since the same object scanned from different positions result in different occlusions, i.e.\, the shadows casted by the object on itself and other objects diverge. The different view angles arise from the scanner's translation relative to the rotation center. For example, the carrier orientations $R_0$ and $R_2$ in Fig.~\ref{fig:redundency} have overlap in the measurements, potentially observing the same surface, but from different positions. These problems are avoided by detecting spherical targets and using only their center points as artificial features to be aligned instead of the entire scanned environment. The estimation of the sphere center points, described in Sec.~\ref{sec:sphereest}, is independent from the view angle onto the sphere and the excerpt seen by the LiDAR. 

The targets are placed in pairs on opposite sides of the platform, thus two spheres are visible at the same time without any further platform rotation (Fig.~\ref{fig:spheredet}, top-left). As mentioned above, after a full carrier rotation each pair of spherical targets has been seen twice. The redundant target pairs are depicted in Fig.~\ref{fig:spheredet}. This redundancy can be used to formulate error measures, which can then be minimized. Obviously, the sphere centers corresponding to the same sphere need to be congruent in the common rigid frame, even if they were seen from different carrier orientations, since they represent the same object. Thus, their divergence indicates how strong the error of a specific sensor pose is. However, since the sensor has six degrees of freedom, minimizing the error can get computationally expensive, especially since all of them need to be optimized together, as long as they are hard coupled. 

To approach this problem, we separated the rotation from the translation part resulting in two error functions depending on the LiDAR's extrinsic parameters. To achieve this the kinematic chains of the targeted platforms along with the relevant transformations are described in Sec.~\ref{sec:kinematic}. After detailing the target and center point detection in Sec.~\ref{sec:sphereest}, the actual decoupling is deduced in Sec.~\ref{sec:decoupling}. Finally, Sec.~\ref{sec:optimization} derives a three dimensional error measure for the rotation and another one for the translation, given that the rotation is already known. Minimizing both with respect to the LiDAR's pose aligns the sphere center points. 

\section{Kinematic Chain} \label{sec:kinematic}
Sensory platforms come with a variety of different constructions and sensor mountings. Our method focuses on platforms with a rotating sensor carrier, on which the LiDARs are seated. Two versions of such architectures are depicted in Fig.~\ref{fig:kinematicchain}. The figure illustrates the transformations between the different frames, emerging from the construction parts of the individual platform. Both drawings, left and right, show the same kinematic chain from the platform's base frame $F_B$ (black) up to the rotating frame of the sensor carrier $F_{R'}$ (red) on which the sensor, with frame $F_S$ (green) is mounted. The only dynamic transformation, referred to as $T_{RR'}$ represents the rotation of the carrier platform. Note that the transformation matrix $T_{RR'}$ is defined as
\begin{equation}
T_{RR'} =
\begin{bmatrix}
R_{RR'} & \vec{t}_{RR'} \\
\begin{matrix}
0 & 0 & 0 \\
\end{matrix} & 1
\end{bmatrix} \in \mathbb{R}^{4x4} ,\ \ \ R_{RR'} \in \mathbb{R}^{3x3} \wedge \vec{t}_{RR'} \in \mathbb{R}^3
\end{equation} 
and $T_{RR'}$ only describes the carrier rotation around its own z-axis, such that $\vec{t}_{RR'} = \vec{0}$. Thus, the translation vector $\vec{t}_{RR'}$ can be ignored and we will refer to $R_{RR'}$ as $R_i,\ i=0,1,...$, since $R_{RR'}$ is constantly changing while the carrier is rotating. $R_i$ represents different orientations of the sensor carrier during the calibration process, as already indicated in Fig.~\ref{fig:redundency}. \\
\begin{wrapfigure}{l}{0.6\textwidth}
	\centering
	\begin{minipage}{0.6\textwidth}
		\includegraphics[width=0.52\textwidth]{fig/kinematic_chain_revolving}
		\includegraphics[width=0.47\textwidth]{fig/kinematic_chain_rotating}
	\end{minipage}
	\caption{
		Simplified kinematic chains of sensing platforms targeted by our method. The left illustration  shows the kinematic chain for a revolving, and the right one for a rotating sensor. Both are equal except for the transformation $T_{RR'}$ from  $F_R$ to $F_{R'}$ (red), which represents the rotating carrier. The transforms $T_{BR}$ and $T_{R'S}$ are static, while $T_{RR'}$ is dynamic, as it represents the actual rotation of the sensor carrier. Note that this transform encodes only rotation and no translation, since it only turns around its own z-axis. $T_{R'S}$ is the sought transformation from the carrier center to the sensor's frame. The difference between left and right is in $T_{R'S}$. While the revolving sensor frame (left) $F_S$ (green) is shifted away from the carrier center, the rotating frame (right) is right above its parent frame $F_R'$. This is why the left one leads to view-point defects in the produced measurement redundancies.}
	\label{fig:kinematicchain}
\end{wrapfigure}
Fig.~\ref{fig:kinematicchain} primarily shows the difference between the kinematic chains of revolving (left) and rotating (right) sensors, and the sought transformation $T_{R'S}$. The sensor pose with respect to $F_{R'}$ is represented as transform $T_{R'S}$. As depicted in Fig.~\ref{fig:kinematicchain}, the left sensor frame $F_S$ in green is shifted away from the rotation center, resulting in an orbital trajectory around the rotation center of the carrier. In contrast, in the right picture, the sensor frame located straight above the rotating carrier frame $F_{R'}$. This observation is further clarified when analyzing the transform $T_{R'S}$. If the translation part of the transform $\vec{t}_{R'S}$, projected on the rotation plane, is zero, then the sensor has to turn around its own center and thus is \textit{rotating}. Otherwise, the sensor is \textit{revolving}.
\begin{equation}
	\begin{Vmatrix}
		\vec{t}_{R'S_x} \\
		\vec{t}_{R'S_y}
	\end{Vmatrix} 
	\left\{
	\begin{array}{@{}ll@{}}
	> 0, & \Rightarrow revolving \\
	= 0, & \Rightarrow rotating
	\end{array}\right.
\end{equation}
The translational shift of revolving scanners also causes the already mentioned view-point errors that impede the direct matching of the redundant data. 

% NOTE: here is a manual pagebrake
\newpage

\section{Sphere Center Estimation} \label{sec:sphereest}
\begin{wrapfigure}{r}{.6\textwidth}
	\centering
	\begin{minipage}{.6\textwidth}
		\centering
		\includegraphics[scale=0.2]{image/manual_guess.png}
		\caption{Configuration of the crop box used to extract the points sampling the spherical targets. The \textit{virtual sphere} depicted in the figure is presented to the user inside the visualization tool RViz. The user can move the sphere to the measurements sampling the calibration targets to configure the cropping before the actual fitting step.}
		\label{fig:manualguess}
	\end{minipage}
\end{wrapfigure}	

	The points that are used in the optimization functions are the center 
	points of the spherical calibration targets. This distinct center points 
	can be determined independently from the excerpt and the view angle onto the 	
	sphere. 
	
	The center points are determined by several fitting and averaging steps. The sensor carrier is rotated into a position from which an opposing pair of spheres can be seen by the LiDAR. Their individual center points are estimated subsequently. However, the spheres can be somewhere in the point cloud recorded by the LiDAR. To avoid a sphere detection method as another source of error, the point cloud is first cropped to the points laying on a certain calibration target. The cropping requires a manual configuration step. After the carrier's rotation angle is adjusted properly to scan certain pair of spheres, the visualization tool RViz \footnote{\href{https://github.com/ros-visualization/rviz}{https://github.com/ros-visualization/rviz}} is used to present the point cloud and a \textit{virtual sphere}, with known and fixed radius $r$ to the user. The virtual sphere can then be moved to the points sampling both of the physical spheres. This is shown in Fig.~\ref{fig:manualguess} for a single sphere. The virtual spheres positions with respect to the sensor frame can be saved to a file and are later be used to compute a bounding box to actually crop the point cloud. This manual configuration has to be done for any carrier orientation $R_i$, from which a pair of targets can be seen by the LiDAR. In the exemplary case of four spheres, i.e.\, two target pairs, there are also four orientations to configure.
	
	As mentioned before, the LiDAR has to see a pair of spheres $S_{p_i}$ and $S_{q_i}$ from a single platform orientation $R_i$. The center $\vec{c} = [c_x, c_y, c_z]^T$ of a certain sphere $S$ is estimated by a least squares model fitting on the previously cropped point cloud 
	$L = \{\vec{l}_0, \vec{l}_1, ..., \vec{l}_{N-1}\}, \vec{l}_i \in \mathbb{R}^3$. 
	The error between a sphere with radius $r$ and a given $\vec{c}$, and a measured point $\vec{l_i} \in L$ define the residuals and is encoded by
	\begin{equation}
		E(\vec{c}, \vec{l}) = \norm{\vec{c} - \vec{l}} - r.
	\end{equation}
	Thus, the global error is represented as a sum of squared residuals 
	\begin{equation}
		G(\vec{c}) = \sum_{i=0}^{N-1} E(\vec{c}, \vec{l}_i)^2
	\end{equation}
	and optimized by
	\begin{equation}
		\vec{c}^{\ \ast} = argmin \left[G(\vec{c}) \right]
	\end{equation}
	The center point $\vec{c}^{\ \ast}$, producing the least overall residual for all points $\vec{l}_i$ laying on the sphere with radius $r$, estimates the real-world sphere center. 
	%The optimization is implemented by an iterative trust region local minimization of the described least square problem.
	
	The fitting is applied to the LiDAR's output, consisting of smaller fragments of an entire sweep. In small, noisy datasets the influence of outliers, e.g.\, by mismeasurements, increase the possibility for the center estimation to be inaccurate.	Therefore, this fitting procedure is done several times on subsequent fragments, containing different target excepts of different size. Averaging the resulting center points should reduce the influence of outliers and too small datasets.
	
	Another possible error arises from the discretization of the actually continuous carrier rotation, done by the encoder used to	acquire the carrier orientation. Driving to an angle which is not an element of the discrete set of possible positions that can be measured, will result in an angular error of up to $\pm0.5$ times the encoder resolution. This in return, has an influence increasing with the distance to the sensor platform. To reduce this, the sphere fitting at the different positions $R_i$ is also repeated by rotating the carrier to the configured orientations multiple times and again averaging the results.

\section{Decoupling} \label{sec:decoupling}
	\begin{wrapfigure}{l}{0.6\textwidth}
		\vspace{-0.5cm}
		\centering
		\begin{minipage}{0.6\textwidth}
			\includegraphics[width=\textwidth]{fig/d_vectors}
		\end{minipage}
		\caption{Direction-vectors between (e.g.\ $\vec{d}_1$ and $\vec{d}_3$) become anti-parallel if the rotation is well known. The picture shows an exemplary target arrangement using $M=4$ spheres. The depicted vectors indicate that the platform is not yet calibrated}
		\label{fig:decouple}
	\end{wrapfigure}
	Minimizing a function with six free parameters can be computationally expensive. Furthermore, in the case of transformation arguments, there are ambiguities between translation and rotation, i.e. some 
	combinations of translations and rotations can compensate others. To tackle these challenges and especially to reduce the problem's dimensionality, we decouple the rotational part from the full six degree of freedom transformation. Inspired by \citep{Kang2016FullDOFCO}, we formulate individual error functions for the rotational and translational part of the LiDAR's extrinsic parameters. To do so, it is assumed that the LiDAR is able to see two spheres $S_{p_i}$ and $S_{q_i}$ in a single carrier position $R_i \in \mathbb{R}^{3x3}$, and that their respective center points $p_i$ and $q_i$ are estimated. We denote the scanners extrinsic parameters as translation $\vec{t} \in \mathbb{R}^3$ and rotation $R \in \mathbb{R}^{3x3}$. It will be the final objective to optimize the respective error functions with respect to $\vec{t}$ and $R$ separately. 
	
	The center points of $S_{p_i}$ and $S_{q_i}$ can be represented in the common, rigid frame $F_R$, by transforming them with the sought sensor transformation $T_{R'S}$ and the measured transformation of the sensor carrier $T_{RR'}$, which, since its translational component is always zero, was previously reduced to $R_i$.
	\begin{align}
		{p'}_i &= R_i*R*(p_i+\vec{t}) \label{eq:p_tf} \\
		{q'}_i &= R_i*R*(q_i+\vec{t}) \label{eq:q_tf}
	\end{align}
	Fig.~\ref{fig:decouple} depicts that the transformed, estimated center points ${p'}_i$ and ${q'}_i$ can be used to construct a vector $\vec{d}_i$ pointing from ${p'}_i$ to ${q'}_i$.
	\begin{equation}
		\vec{d}_i = {p'}_i - {q'}_i \label{eq:dvec}
	\end{equation}
	Inserting Eq.~\ref{eq:p_tf} and Eq.~\ref{eq:q_tf} in Eq.~\ref{eq:dvec} results in,
	%\begin{align}		
		\begin{align}		
			\vec{d}_i & = R_i*R*(p_i+\vec{t}) - (R_i*R*(q_i+\vec{t})) \\
			 & = R_i*R*p_i + R_i*R*\vec{t} - (R_i*R*q_i + R_i*R*\vec{t}) \\
			 %& = R_i*R*p_i + R_i*R*\vec{t} - R_i*R*q_i - R_i*R*\vec{t} \\
			 %& = R_i*R*p_i - R_i*R*q_i + R_i*R*\vec{t} - R_i*R*\vec{t} \\
			 & = R_i*R*p_i - R_i*R*q_i \\
			 & = R_i*R*(p_i - q_i). \label{eq:decoupled}
		\end{align}
	%\end{align}
	Thus, the vectors $\vec{d}_i$ between the center points of a pair of spheres, seen with a carrier orientation $R_i$ do not depend on the LiDAR's translation, relative to the carrier's rotation center. 
	
	This leads to another observation of two such vectors $\vec{d}_i$ and $\vec{d}_j$ between the same pair of spheres, but seen with different carrier orientations $R_i$ and $R_j$, respectively, such as $\vec{d}_0$ and $\vec{d}_2$ as illustrated in Fig.~\ref{fig:decouple}. Due to the redundancy in the LiDAR's measurements, both vectors have to point in exactly opposite directions, when they are transformed into a common frame. This is because both measurements include the same spheres with once $S_{p_i}$ on the left of the sensor and from the other orientation with $S_{q_i}$ on the left. This means that
	\begin{equation}
		\vec{d}_i = -\vec{d}_j
		\label{eq:antiparallel}
	\end{equation}
	with $j = i + (M/2)$ has to hold, where $M$ is the number of spheres. This insight is used in Sec.~\ref{sec:optimization} to formulate a rotational error of the sensor's pose.
	
\section{Optimization} \label{sec:optimization}
	As mentioned before the LiDAR's rotation is optimized separately from its translation. The vectors $\vec{d}_i$ and $\vec{d}_j$, between a pair of sphere centers estimated from two different carrier orientations $R_i$ and $R_j$, need to be anti-parallel when the configured extrinsic parameters of the sensor are close or equal to the true pose. This is exploited by formulating the error of a single pair of spheres as the magnitude of the cross-product of $\vec{d}_i$ and $\vec{d}_j$. Recall that the cross product's norm can be formulated as 
	\begin{equation}
		||\vec{d_i}\times(-\vec{d_j})|| = ||\vec{d_i}||*||\vec{d_j}||*\sin(\phi),\ \ \ \phi = \sphericalangle(\vec{d_i},(\vec{d_j})).
	\end{equation}
	Thus the norm directly depends on the angle $\phi$ between the two vectors $\vec{d}_i$ and $\vec{d}_j$, which should be anti-parallel, i.e.\, $\phi = \pi$. It is also expected, that the magnitude of the cross-product gets more sensitive to changes in $\phi$, the higher the norm of the individual $\vec{d}$ vectors are. This observation leads to an error function for a single pair of spheres.
	\begin{align}
		E_{ij}(R) & = ||\vec{d}_i \times (\vec{d}_j)|| \label{eq:sglerr} \\
				  & = ||R_i*R*(p_i - q_i) \times R_j*R*(p_j - q_j)|| \label{eq:sglerr_explicit} \\
	\end{align}
	As can be seen in Eq.~\ref{eq:sglerr_explicit} $R_i$, $p_i$, $q_i$, $R_j$, $p_j$ and $q_j$ are all measured quantities, and so the error of the vector pair $\vec{d}_i$ and $\vec{d}_j$, only depends on the LiDAR orientation $R$. Also the cross-product's norm of two anti-parallel vectors gets zero, so it works well as minimization objective in the first step. 
	Thus, an overall error for the rotational part $R$ of $T_{R'S}$ can be formulated as
	\begin{equation}
		E(R) = \sum_{i=0}^{M/2} E_{ij}(R),
	\end{equation}
	where, again, $j = i + \frac{M}{2}$ and $M$ is the number of spheres. The orientation $R^{\ast}$ that minimizes $E(R)$ approximates the LiDAR's actual orientation:
	\begin{equation}
		R^{\ast} = argmin\left[E(R)\right].
	\end{equation}		
	Afterwards, the translation is optimized separately. The sphere centers representing the same target can be used to formulate the error of a given sensor translation $\vec{t}_S$. As the orientation $R^{\ast}$ has been estimated previously, the translation error can be formulated as 
	\begin{align}
		E_{ij}(\vec{t}) & = p'_i - q'_j \label{eq:transerr} \\
						& = R_i*R^{\ast}*(p_i+\vec{t}) - R_j*R^{\ast}*(q_j+\vec{t}), 
						\label{eq:transerr_explicit}.
	\end{align}
	Again, $R_i$, $p_i$, $R_i$, and $q_j$ are measured. Furthermore, note that $R^{\ast}$ is assumed to have been estimated accurately. Therefore, $\vec{t}_S$ is the only independent variable. As already done for the LiDAR's orientation, Eq.~\ref{eq:transerr_explicit} can be used to formulate a least square problem
	\begin{equation}
		E(\vec{t}) = \sum_{i=0}^{M-1} E_{ij}(\vec{t})^2.
	\end{equation}
	The actual translation with respect to the carrier's rotation center $\vec{t}^{\ \ast}$ is estimated as:
	\begin{equation}
		\vec{t}^{\ \ast} = argmin\left[E(\vec{t})\right].
	\end{equation}	
	Both error functions have to be minimized with respect to their respective argument. In our experiments, attempts to do this via classic gradient descent or trust region methods often converged to one of the many local minima, and thus did not result in the desired global minimum. Thus, a global minimization approach seems better suited. A simple grid search to determine global minima gets tractable due to the reduction of the dimensionality of the original problem, achieved by decoupling the orientation and translation of the six degree of freedom pose. However, this still leads to cubic complexity of both objective functions, if no further optimizations are performed. 
	
	A further dimension reduction is possible for the translation optimization, since the $z$ component of $\vec{t}_S$ can not be found. Changes in $z$ will just lift or lower all measurements equally and therefore will have no effect on the error function. Ignoring $t_{S_z}$ will reduce the translation search to quadratic complexity. 
	
	Note that although, the complexity of the optimization of the sensor orientation remains cubic. It can get tractable, in practice by reducing the parameter grid's resolution. This, however is also a problem, because the exact global minimum will probably be missed. Therefore, this is tackled with a subsequent local optimization using an gradient descent solver which uses promising candidates resulting from the grid search as starting points.